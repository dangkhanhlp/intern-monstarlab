import { ProjectUser } from '../entities/project-user.entity';
import { EntityRepository, Repository } from 'typeorm';

@EntityRepository(ProjectUser)
export class ProjectUserRepository extends Repository<ProjectUser> {}
