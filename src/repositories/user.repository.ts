import { NotFoundException } from '@nestjs/common';
import { User } from '../entities/user.entity';
import { EntityRepository, Repository } from 'typeorm';
import * as crypto from 'crypto';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  async findById(id: number): Promise<User> {
    const user = await this.findOne({
      where: { id: id },
    });
    if (!user) {
      throw new NotFoundException('user is not exist in database');
    }
    return user;
  }
  convertToMD5(str: string): string {
    const md5Hash = crypto.createHash('md5');
    md5Hash.update(str);
    return md5Hash.digest('hex');
  }
}
