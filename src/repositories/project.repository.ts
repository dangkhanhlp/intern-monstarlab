import { NotFoundException } from '@nestjs/common';
import { Project } from '../entities/projects.entity';
import { EntityRepository, Repository } from 'typeorm';

@EntityRepository(Project)
export class ProjectRepository extends Repository<Project> {
  async findById(id: number): Promise<Project> {
    const project = await this.findOne({
      where: { id: id },
    });
    if (!project) {
      throw new NotFoundException('project is not exist in database');
    }
    return project;
  }
}
