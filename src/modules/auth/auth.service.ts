import { BadRequestException, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserDto } from 'src/dto/user.dto';
import { UserService } from '../users/user.service';

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
  ) {}
  async validateUser(userName: string, password: string): Promise<any> {
    const user = await this.userService.findOne(userName);
    if (user && user.password === password) {
      const { password, ...result } = user;
      return result;
    }
    throw new BadRequestException('Invalid user');
  }
  async login(user: UserDto) {
    const { userName, password } = user;
    const payload = { userName };
    await this.validateUser(userName, password);
    const token = this.jwtService.sign(payload);
    return {
      access_token: token,
      expires_in: '1h',
    };
  }
}
