import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ProjectUserController } from './project-user.controller';
import { ProjectUserService } from './project-user.service';
import { ProjectUserRepository } from 'src/repositories/project-user.repository';
import { UserRepository } from 'src/repositories/user.repository';
import { ProjectRepository } from 'src/repositories/project.repository';
import { AuthMiddleware } from 'src/middlewares/auth.middleware';
import { JwtModule } from '@nestjs/jwt';
import { jwtConstants } from '../auth/constant';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      ProjectUserRepository,
      UserRepository,
      ProjectRepository,
    ]),
    JwtModule.register(jwtConstants),
  ],
  controllers: [ProjectUserController],
  providers: [ProjectUserService],
  exports: [ProjectUserService],
})
export class ProjectUserModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(AuthMiddleware).forRoutes(ProjectUserController);
  }
}
