import { Injectable, NotFoundException } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { ProjectUserDto } from '../../dto/project-user.dto';
import { ProjectUser } from '../../entities/project-user.entity';
import { ProjectUserRepository } from '../../repositories/project-user.repository';
import { ProjectRepository } from '../../repositories/project.repository';
import { UserRepository } from '../../repositories/user.repository';

@Injectable()
export class ProjectUserService {
  constructor(
    private readonly projectUserRepository: ProjectUserRepository,
    private readonly userRepository: UserRepository,
    private readonly projectRepository: ProjectRepository,
  ) {}

  async getProjectUserById(projectUserId: number): Promise<ProjectUser> {
    const projectUser = await this.projectUserRepository.findOne(projectUserId);
    if (!projectUser) {
      throw new NotFoundException();
    }
    return projectUser;
  }

  async getAllProjectUsers(): Promise<ProjectUser[]> {
    return this.projectUserRepository.find();
  }

  async createProjectUser(
    projectUserDto: ProjectUserDto,
  ): Promise<ProjectUserDto> {
    await this.userRepository.findById(projectUserDto.userId);
    await this.projectRepository.findById(projectUserDto.projectId);
    const projectUser = plainToClass(ProjectUser, projectUserDto);
    return await this.projectUserRepository.save(projectUser);
  }

  async updateProjectUser(id: number, projectUserDto: ProjectUserDto) {
    await this.userRepository.findById(projectUserDto.userId);
    await this.projectRepository.findById(projectUserDto.projectId);
    const projectUser = plainToClass(ProjectUser, projectUserDto);
    return await this.projectUserRepository.update(id, projectUser);
  }

  async deleteProjectUser(id: number) {
    await this.projectUserRepository.delete(id);
  }
}
