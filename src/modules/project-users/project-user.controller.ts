import {
  Controller,
  Get,
  Post,
  Patch,
  Delete,
  Param,
  Body,
} from '@nestjs/common';
import { ProjectUserService } from './project-user.service';
import { ProjectUserDto } from 'src/dto/project-user.dto';
import { ProjectUser } from 'src/entities/project-user.entity';

@Controller('api/project-users')
export class ProjectUserController {
  constructor(private readonly projectUserService: ProjectUserService) {}

  @Get(':id')
  async getById(@Param('id') id: number): Promise<ProjectUser> {
    return this.projectUserService.getProjectUserById(id);
  }
  @Get()
  async getAll() {
    return this.projectUserService.getAllProjectUsers();
  }

  @Post()
  create(@Body() projectUserDto: ProjectUserDto) {
    return this.projectUserService.createProjectUser(projectUserDto);
  }

  @Patch(':id')
  async update(
    @Param('id') id: number,
    @Body() projectUserDto: ProjectUserDto,
  ) {
    console.log(id);
    await this.projectUserService.updateProjectUser(id, projectUserDto);
  }

  @Delete(':id')
  async delete(@Param('id') id: number) {
    await this.projectUserService.deleteProjectUser(id);
  }
}
