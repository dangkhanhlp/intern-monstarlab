import { Injectable, NotFoundException, Res } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { UserDto } from '../../dto/user.dto';
import { User } from '../../entities/user.entity';
import { UserRepository } from '../../repositories/user.repository';
import { Project } from '../../entities/projects.entity';
import * as fs from 'fs';
import * as csv from 'fast-csv';

@Injectable()
export class UserService {
  constructor(private readonly userRepository: UserRepository) {}

  async getUserById(userId: number): Promise<User> {
    const user = await this.userRepository.findOne(userId);
    if (!user) {
      throw new NotFoundException();
    }
    return user;
  }
  async getAllUsers(): Promise<User[]> {
    return this.userRepository.find();
  }
  async createUser(userDto: UserDto): Promise<UserDto> {
    const user = plainToClass(User, userDto);
    user.password = this.userRepository.convertToMD5(user.password);
    return await this.userRepository.save(user);
  }
  async updateUser(id: number, userDto: UserDto) {
    const user = plainToClass(User, userDto);
    user.password = this.userRepository.convertToMD5(user.password);
    return await this.userRepository.update(id, user);
  }
  async deleteUser(id: number) {
    await this.userRepository.delete(id);
  }

  async getProjectsByUserId(userId: number): Promise<Project[]> {
    const user = await this.userRepository.findOne(userId, {
      relations: ['projects'],
    });
    if (!user) {
      throw new NotFoundException();
    }
    return user.projects;
  }
  async exportProjectsByUserId(userId: number, @Res() res) {
    const projects = await this.getProjectsByUserId(userId);
    const header = [
      'No',
      'name',
      'category',
      'projected_spend',
      'projected_variance',
      'revenue_recognised',
    ];
    const csvStream = csv.format({ headers: header });
    const writeStream = fs.createWriteStream(
      './src/file/projects_' + userId + '.csv',
    );
    csvStream.pipe(writeStream);

    projects.forEach((project, index) => {
      csvStream.write([
        index + 1,
        project.name,
        project.category,
        project.projectedSpend,
        project.projectedVariance,
        project.revenueRecognised,
      ]);
    });
    const readStream = fs.createReadStream(
      './src/file/projects_' + userId + '.csv',
    );
    res.header('Content-Type', 'text/csv');
    res.header(
      'Content-Disposition',
      `attachment; filename = projects_${userId}.csv`,
    );
    readStream.pipe(res);
    csvStream.end();
  }
  async findOne(userName: string): Promise<User> {
    return this.userRepository.findOne({ where: { userName: userName } });
  }
}
