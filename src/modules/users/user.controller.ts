import {
  Controller,
  Get,
  Param,
  Post,
  Patch,
  Delete,
  Body,
  Res,
} from '@nestjs/common';
import { UserDto } from 'src/dto/user.dto';
import { User } from 'src/entities/user.entity';
import { UserService } from './user.service';

@Controller('api/users')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get(':id')
  async getById(@Param('id') id: number): Promise<User> {
    return this.userService.getUserById(id);
  }
  @Get()
  async getAll() {
    return this.userService.getAllUsers();
  }

  @Post()
  create(@Body() userDto: UserDto) {
    return this.userService.createUser(userDto);
  }

  @Patch(':id')
  async update(@Param('id') id: number, @Body() userDto: UserDto) {
    await this.userService.updateUser(id, userDto);
  }

  @Delete(':id')
  async delete(@Param('id') id: number) {
    await this.userService.deleteUser(id);
  }

  @Get(':id/projects')
  async getProjects(@Param('id') userId: number) {
    return this.userService.getProjectsByUserId(userId);
  }
  @Get(':id/exportcsv/projects')
  async exportCsvFile(@Param('id') userId: number, @Res() res) {
    return this.userService.exportProjectsByUserId(userId, res);
  }
}
