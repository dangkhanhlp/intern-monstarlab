import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { InputDto } from 'src/dto/input';
import { ProjectDto } from 'src/dto/project.dto';
import { Project } from 'src/entities/projects.entity';
import { ProjectService } from './project.service';

@Controller('api/app/projects')
export class ProjectController {
  constructor(private readonly projectService: ProjectService) {}

  @Get(':id')
  async getById(@Param('id') id: number): Promise<Project> {
    return this.projectService.getProjectById(id);
  }

  @Get()
  async getAll(@Query() inputDto: InputDto) {
    return this.projectService.getAllProjects(inputDto);
  }

  @Post()
  create(@Body() projectDto: ProjectDto) {
    return this.projectService.createProject(projectDto);
  }

  @Patch(':id')
  async update(@Param('id') id: number, @Body() projectDto: ProjectDto) {
    await this.projectService.updateProject(id, projectDto);
  }

  @Delete(':id')
  async delete(@Param('id') id: number) {
    await this.projectService.deleteProject(id);
  }
}
