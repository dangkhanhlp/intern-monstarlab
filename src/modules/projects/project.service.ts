import { Injectable, NotFoundException } from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { ProjectDto } from 'src/dto/project.dto';
import { ProjectRepository } from '../../repositories/project.repository';
import { Project } from '../../entities/projects.entity';
import { InputDto } from 'src/dto/input';

@Injectable()
export class ProjectService {
  constructor(private readonly projectRepository: ProjectRepository) {}

  async getProjectById(projectId: number): Promise<Project> {
    const project = await this.projectRepository.findOne(projectId);
    if (!project) {
      throw new NotFoundException();
    }
    return project;
  }

  async getAllProjects(inputDto: InputDto) {
    let currentPage: number = Number(inputDto.page);
    let limitItem: number = Number(inputDto.limitItem);
    if (!limitItem) {
      limitItem = 3;
    }
    if (currentPage < 1) {
      currentPage = 1;
    }
    const totalRow = await (await this.projectRepository.find()).length;
    const totalPage: number = Math.ceil(totalRow / limitItem);
    if (currentPage > totalPage) {
      currentPage = totalPage;
    }
    const skip: number = (currentPage - 1) * limitItem;
    const take: number = limitItem;
    const projects = await this.projectRepository.find({
      skip: skip,
      take: take,
    });
    return {
      items: projects,
      currentPage: currentPage,
      totalPage: totalPage,
    };
  }

  async createProject(projectDto: ProjectDto): Promise<ProjectDto> {
    const project = plainToClass(Project, projectDto);
    return await this.projectRepository.save(project);
  }

  async updateProject(id: number, projectDto: ProjectDto) {
    const project = plainToClass(Project, projectDto);
    return await this.projectRepository.update(id, project);
  }
  async deleteProject(id: number) {
    await this.projectRepository.delete(id);
  }
}
