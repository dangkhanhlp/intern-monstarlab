import { IsNotEmpty, IsInt } from 'class-validator';

export class ProjectUserDto {
  @IsNotEmpty()
  @IsInt()
  projectId: number;

  @IsNotEmpty()
  @IsInt()
  userId: number;
}
