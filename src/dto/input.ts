import { IsNumberString, IsOptional } from 'class-validator';
export class InputDto {
  @IsNumberString()
  @IsOptional()
  page: number;

  @IsNumberString()
  @IsOptional()
  limitItem: number;
}
