import { IsBoolean, IsNotEmpty, IsString, IsStrongPassword, MaxLength } from 'class-validator';

export class UserDto {
  @IsNotEmpty()
  @IsString()
  @MaxLength(255)
  firstName: string;

  @IsNotEmpty()
  @IsString()
  @MaxLength(255)
  lastName: string;

  @IsNotEmpty()
  @IsString()
  @MaxLength(30)
  userName: string;

  @IsNotEmpty()
  @IsStrongPassword()
  password: string;

  @IsBoolean()
  @IsNotEmpty()
  isActive: boolean;
}
