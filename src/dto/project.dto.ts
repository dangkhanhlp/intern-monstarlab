import { Type } from 'class-transformer';
import {
  IsDate,
  IsInt,
  IsEnum,
  IsNotEmpty,
  IsString,
  Length,
} from 'class-validator';
import { Category } from '../enum/category.validate.enum';
import { IsBiggerThan } from '../validations/validate.project-ended-at';
import { IsBiggerThanNow } from '../validations/validate.project-started-at';

export class ProjectDto {
  @IsString()
  @IsNotEmpty()
  @Length(1, 255)
  name: string;

  @IsString()
  @IsEnum(Category)
  category: string;

  @IsInt()
  projectedSpend: number;

  @IsInt()
  projectedVariance: number;

  @IsDate()
  @Type(() => Date)
  @IsBiggerThanNow({
    message: 'projectStartedAt must be bigger than current time',
  })
  projectStartedAt: Date;

  @IsDate()
  @Type(() => Date)
  @IsBiggerThan('projectStartedAt', {
    message: 'projectEndedAt must be bigger than projectStartedAt',
  })
  projectEndedAt?: Date;
}
