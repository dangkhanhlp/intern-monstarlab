import { Test, TestingModule } from '@nestjs/testing';
import { ProjectRepository } from '../../repositories/project.repository';
import { ProjectService } from '../../modules/projects/project.service';
import { NotFoundException } from '@nestjs/common';
import { ProjectDto } from '../../dto/project.dto';
import { InputDto } from '../../dto/input';

describe('ProjectService', () => {
  let service: ProjectService;

  const mockRepository = {
    find: jest.fn(),
    save: jest.fn(),
    delete: jest.fn(),
    findOne: jest.fn(),
    update: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ProjectService,
        {
          provide: ProjectRepository,
          useValue: mockRepository,
        },
      ],
    }).compile();

    service = module.get<ProjectService>(ProjectService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
  describe('getAllProjects', () => {
    it('should return an array of projects', () => {
      const input = new InputDto();
      input.page = 1;
      input.limitItem = 3;
      const projects = [
        {
          id: 1,
          name: 'Project 1',
          category: 'system',
          projectedSpend: 5000,
          projectedVariance: 1000,
          revenueRecognised: 0,
          projectStartedAt: '2023-03-20T00:00:00.000Z',
          projectEndedAt: '2024-01-30T00:00:00.000Z',
          createdAt: '2023-02-03T03:57:45.000Z',
          updatedAt: '2023-02-03T03:57:45.000Z',
          deletedAt: null,
        },
        {
          id: 2,
          name: 'Project 2',
          category: 'system',
          projectedSpend: 5000,
          projectedVariance: 1000,
          revenueRecognised: 0,
          projectStartedAt: '2023-03-20T00:00:00.000Z',
          projectEndedAt: '2024-01-30T00:00:00.000Z',
          createdAt: '2023-02-03T03:57:50.000Z',
          updatedAt: '2023-02-03T03:57:50.000Z',
          deletedAt: null,
        },
        {
          id: 3,
          name: 'Project 3',
          category: 'system',
          projectedSpend: 5000,
          projectedVariance: 1000,
          revenueRecognised: 0,
          projectStartedAt: '2023-03-20T00:00:00.000Z',
          projectEndedAt: '2024-01-30T00:00:00.000Z',
          createdAt: '2023-02-03T03:57:55.000Z',
          updatedAt: '2023-02-03T03:57:55.000Z',
          deletedAt: null,
        },
      ];
      mockRepository.find.mockResolvedValue(projects);
      expect(service.getAllProjects(input)).resolves.toEqual({
        items: projects,
        currentPage: 1,
        totalPage: 1,
      });
    });
  });
  describe('createProject', () => {
    it('should create a new project', async () => {
      const project = { name: 'Project 100' };
      mockRepository.save.mockResolvedValue(project);
      expect(service.createProject(project as ProjectDto)).resolves.toEqual(
        project,
      );
    });
  });

  describe('getProjectById', () => {
    it('should return a project with the specified id', async () => {
      const project = {
        id: 1,
        name: 'Project 1',
        category: 'system',
        projectedSpend: 5000,
        projectedVariance: 1000,
        revenueRecognised: 0,
        projectStartedAt: '2023-03-20T00:00:00.000Z',
        projectEndedAt: '2024-01-30T00:00:00.000Z',
        createdAt: '2023-02-03T03:57:45.000Z',
        updatedAt: '2023-02-03T03:57:45.000Z',
        deletedAt: null,
      };
      mockRepository.findOne.mockResolvedValue(project);
      expect(service.getProjectById(1)).resolves.toEqual(project);
    });
    it('should throw NotFoundException', async () => {
      mockRepository.findOne.mockRejectedValue(new NotFoundException());
      const id = 0;
      await expect(service.getProjectById(id)).rejects.toThrow(
        NotFoundException,
      );
    });
  });
  describe('updateProject', () => {
    it('should update project for given id', async () => {
      const updateProjectBody = {
        projectedSpend: 1,
        projectedVariance: 1,
      };
      mockRepository.update.mockResolvedValue(updateProjectBody);

      expect(
        service.updateProject(1, updateProjectBody as ProjectDto),
      ).resolves.toEqual(updateProjectBody);
      // expect(mockRepository.update).toHaveBeenCalledTimes(1);
    });
  });
  describe('deleteProject', () => {
    it('should delete project for given id', async () => {
      const id = 1;
      await service.deleteProject(id);
      expect(mockRepository.delete).toHaveBeenCalledWith(id);
    });
  });
});
