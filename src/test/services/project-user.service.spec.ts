import { Test, TestingModule } from '@nestjs/testing';
import { ProjectRepository } from '../../repositories/project.repository';
import { UserRepository } from '../../repositories/user.repository';
import { ProjectUserService } from '../../modules/project-users/project-user.service';
import { ProjectUserRepository } from '../../repositories/project-user.repository';
import { NotFoundException } from '@nestjs/common';
import { ProjectUserDto } from '../../dto/project-user.dto';

describe('ProjectUserService', () => {
  let service: ProjectUserService;

  const mockRepository = {
    find: jest.fn(),
    findById: jest.fn(),
    save: jest.fn(),
    delete: jest.fn(),
    findOne: jest.fn(),
    update: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ProjectUserService,
        {
          provide: ProjectUserRepository,
          useValue: mockRepository,
        },
        {
          provide: ProjectRepository,
          useValue: mockRepository,
        },
        {
          provide: UserRepository,
          useValue: mockRepository,
        },
      ],
    }).compile();
    service = module.get<ProjectUserService>(ProjectUserService);
  });
  it('should be defined', () => {
    expect(service).toBeDefined();
  });
  describe('getAllProjectUsers', () => {
    it('should return all project_users', () => {
      const projectUsers = {
        id: 3,
        projectId: 1,
        userId: 2,
        createdAt: '2023-02-03T04:16:36.000Z',
        updatedAt: '2023-02-03T04:16:36.000Z',
        deletedAt: null,
      };
      mockRepository.find.mockResolvedValue(projectUsers);
      expect(service.getAllProjectUsers()).resolves.toEqual(projectUsers);
    });
  });

  describe('getProjectUserById', () => {
    it('should return a project_user with the specified id', () => {
      const projectUser = {
        id: 3,
        projectId: 1,
        userId: 2,
        createdAt: '2023-02-03T04:16:36.000Z',
        updatedAt: '2023-02-03T04:16:36.000Z',
        deletedAt: null,
      };
      mockRepository.findOne.mockResolvedValue(projectUser);
      expect(service.getProjectUserById(1)).resolves.toEqual(projectUser);
    });
    it('should throw NotFoundException', async () => {
      mockRepository.findOne.mockRejectedValue(new NotFoundException());
      const id = 0;
      await expect(service.getProjectUserById(id)).rejects.toThrow(
        NotFoundException,
      );
    });
  });

  describe('createProjectUser', () => {
    it('should create a new project_user', async () => {
      const projectUser = {
        projectId: 4,
        userId: 5,
      };
      mockRepository.save.mockResolvedValue(projectUser);
      expect(
        service.createProjectUser(projectUser as ProjectUserDto),
      ).resolves.toEqual(projectUser);
    });
    it('should throw NotFoundException', async () => {
      mockRepository.findById.mockRejectedValue(new NotFoundException());
      const projectUser = new ProjectUserDto();
      projectUser.projectId = 0;
      projectUser.userId = 0;
      await expect(service.createProjectUser(projectUser)).rejects.toThrow(
        NotFoundException,
      );
      expect(mockRepository.findById).toHaveBeenCalled();
    });
  });

  describe('updateProjectUser', () => {
    it('should update a project_user by given id', () => {
      const updateProjectUserBody = {
        projectId: 2,
        userId: 2,
      };
      mockRepository.update.mockResolvedValue(
        updateProjectUserBody as ProjectUserDto,
      );

      expect(
        service.updateProjectUser(1, updateProjectUserBody as ProjectUserDto),
      ).resolves.toEqual(updateProjectUserBody);
    });
    it('should throw NotFoundException', async () => {
      mockRepository.findById.mockRejectedValue(new NotFoundException());
      const updateProjectUserBody = {
        projectId: 0,
        userId: 0,
      };
      await expect(
        service.updateProjectUser(1, updateProjectUserBody),
      ).rejects.toThrow(NotFoundException);
      expect(mockRepository.findById).toHaveBeenCalled();
    });
  });

  describe('deleteProjectUser', () => {
    it('should delete a project_user by given id', async () => {
      const id = 1;
      await service.deleteProjectUser(id);
      expect(mockRepository.delete).toHaveBeenCalledWith(id);
    });
  });
});
