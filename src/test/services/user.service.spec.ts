import { Test, TestingModule } from '@nestjs/testing';
import { UserRepository } from '../../repositories/user.repository';
import { NotFoundException } from '@nestjs/common';
import { UserService } from '../../modules/users/user.service';
import { UserDto } from 'src/dto/user.dto';

describe('ProjectService', () => {
  let service: UserService;

  const mockRepository = {
    find: jest.fn(),
    save: jest.fn(),
    delete: jest.fn(),
    findOne: jest.fn(),
    update: jest.fn(),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UserService,
        {
          provide: UserRepository,
          useValue: mockRepository,
        },
      ],
    }).compile();

    service = module.get<UserService>(UserService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
  describe('getAllUsers', () => {
    it('should return an array of users', () => {
      const users = {
        id: 1,
        firstName: 'Nguyen Dang',
        lastName: 'Khanh',
        isActive: 1,
        createdAt: '2023-02-02T10:21:34.820Z',
        updatedAt: '2023-02-02T10:24:10.000Z',
      };
      mockRepository.find.mockResolvedValue(users);
      expect(service.getAllUsers()).resolves.toEqual(users);
    });
  });
  describe('createUser', () => {
    it('should create a new user', async () => {
      const user = {
        firstName: 'Nguyen',
        lastName: 'Khanh Dang',
        isActive: true,
      };
      mockRepository.save.mockResolvedValue(user);
      expect(service.createUser(user as UserDto)).resolves.toEqual(user);
    });
  });

  describe('getUserById', () => {
    it('should return a user with the specified id', () => {
      const user = {
        id: 1,
        firstName: 'Nguyen Dang',
        lastName: 'Khanh',
        isActive: 1,
        createdAt: '2023-02-02T10:21:34.820Z',
        updatedAt: '2023-02-02T10:24:10.000Z',
      };
      mockRepository.findOne.mockResolvedValue(user);
      expect(service.getUserById(1)).resolves.toEqual(user);
    });
    it('should throw NotFoundException', async () => {
      mockRepository.findOne.mockRejectedValue(new NotFoundException());
      const id = 0;
      await expect(service.getUserById(id)).rejects.toThrow(NotFoundException);
    });
  });
  describe('updateUser', () => {
    it('should update user for given id', async () => {
      const updateUserBody = {
        lastName: 'Huyen',
      };
      mockRepository.update.mockResolvedValue(updateUserBody);
      expect(service.updateUser(1, updateUserBody as UserDto)).resolves.toEqual(
        updateUserBody,
      );
    });
  });
  describe('deleteUser', () => {
    it('should delete user for given id', async () => {
      const id = 1;
      await service.deleteUser(id);
      expect(mockRepository.delete).toHaveBeenCalledWith(id);
    });
  });

  describe('getProjectByUserId', () => {
    it('should return an array of projects for given userId', async () => {
      const userProjects = {
        id: 2,
        name: 'Project 2',
        category: 'system',
        projectedSpend: 5000,
        projectedVariance: 1000,
        revenueRecognised: 0,
        projectStartedAt: '2023-03-20T00:00:00.000Z',
        projectEndedAt: '2024-01-30T00:00:00.000Z',
        createdAt: '2023-02-03T03:57:50.000Z',
        updatedAt: '2023-02-03T03:57:50.000Z',
        deletedAt: null,
      };
      mockRepository.findOne.mockResolvedValue(userProjects);
      expect(service.getUserById(1)).resolves.toEqual(userProjects);
    });
    it('should throw NotFoundException', async () => {
      mockRepository.findOne.mockRejectedValue(new NotFoundException());
      const id = 1;
      await expect(service.getProjectsByUserId(id)).rejects.toThrow(
        NotFoundException,
      );
    });
  });
});
