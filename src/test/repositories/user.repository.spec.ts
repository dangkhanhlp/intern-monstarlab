import { Test, TestingModule } from '@nestjs/testing';
import { NotFoundException } from '@nestjs/common';
import { User } from '../../entities/user.entity';
import { UserRepository } from '../../repositories/user.repository';

describe('UserRepository', () => {
  let userRepository: UserRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [UserRepository],
    }).compile();

    userRepository = module.get<UserRepository>(UserRepository);
  });

  describe('findById', () => {
    it('should return a user', async () => {
      const user = new User();
      user.id = 1;
      jest.spyOn(userRepository, 'findOne').mockResolvedValue(user);

      const result = await userRepository.findById(1);

      expect(result).toEqual(user);
    });

    it('should throw a NotFoundException if user is not exist in database', async () => {
      jest.spyOn(userRepository, 'findOne').mockResolvedValue(null);

      expect(userRepository.findById(1)).rejects.toThrow(NotFoundException);
    });
  });
  describe('findAll', () => {
    it('should return an array of users', async () => {
      const users = [
        {
          id: 1,
          firstName: 'Nguyen Dang',
          lastName: 'Khanh',
          isActive: 1,
          createdAt: '2023-02-02T10:21:34.820Z',
          updatedAt: '2023-02-02T10:24:10.000Z',
        },
      ];
      jest.spyOn(userRepository, 'find').mockResolvedValue(users as any);

      const result = await userRepository.find();

      expect(result).toEqual(users);
    });
  });
});
