import { Test, TestingModule } from '@nestjs/testing';
import { NotFoundException } from '@nestjs/common';
import { ProjectRepository } from '../../repositories/project.repository';
import { Project } from '../../entities/projects.entity';

describe('UserRepository', () => {
  let projectRepository: ProjectRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ProjectRepository],
    }).compile();

    projectRepository = module.get<ProjectRepository>(ProjectRepository);
  });

  describe('findById', () => {
    it('should return a project', async () => {
      const project = new Project();
      project.id = 1;
      jest.spyOn(projectRepository, 'findOne').mockResolvedValue(project);

      const result = await projectRepository.findById(1);

      expect(result).toEqual(project);
    });

    it('should throw a NotFoundException if user is not exist in database', async () => {
      jest.spyOn(projectRepository, 'findOne').mockResolvedValue(null);

      expect(projectRepository.findById(1)).rejects.toThrow(NotFoundException);
    });
  });
  describe('findAll', () => {
    it('should return an array of projects', async () => {
      const projects = [
        {
          id: 1,
          name: 'Project 1',
          category: 'system',
          projectedSpend: 5000,
          projectedVariance: 1000,
          revenueRecognised: 0,
          projectStartedAt: '2023-03-20T00:00:00.000Z',
          projectEndedAt: '2024-01-30T00:00:00.000Z',
          createdAt: '2023-02-03T03:57:45.000Z',
          updatedAt: '2023-02-03T03:57:45.000Z',
          deletedAt: null,
        },
      ];
      jest.spyOn(projectRepository, 'find').mockResolvedValue(projects as any);

      const result = await projectRepository.find();

      expect(result).toEqual(projects);
    });
  });
});
