import { Test, TestingModule } from '@nestjs/testing';
import { ProjectUserRepository } from '../../repositories/project-user.repository';
import { ProjectUser } from '../../entities/project-user.entity';

describe('ProjectUserRepository', () => {
  let projectUserRepository: ProjectUserRepository;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ProjectUserRepository],
    }).compile();

    projectUserRepository = module.get<ProjectUserRepository>(
      ProjectUserRepository,
    );
  });

  describe('findAll', () => {
    it('should return an array of project users', async () => {
      const projectUsers = [
        { id: 1, projectId: 1, userId: 1 },
        { id: 2, projectId: 1, userId: 2 },
      ];
      jest
        .spyOn(projectUserRepository, 'find')
        .mockResolvedValue(projectUsers as any);

      const result = await projectUserRepository.find();

      expect(result).toEqual(projectUsers);
    });
  });
});
