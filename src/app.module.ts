import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { User } from './entities/user.entity';
import { UserModule } from './modules/users/user.module';
import { ProjectController } from './modules/projects/project.controller';
import { ProjectModule } from './modules/projects/project.module';
import { Project } from './entities/projects.entity';
import { ProjectUser } from './entities/project-user.entity';
import { ProjectUserController } from './modules/project-users/project-user.controller';
import { ProjectUserModule } from './modules/project-users/project-user.module';
import { AuthController } from './modules/auth/auth.controller';
import { AuthModule } from './modules/auth/auth.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.DB_HOST,
      port: Number.parseInt(process.env.DB_PORT),
      username: process.env.DB_USERNAME,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME,
      entities: [User, Project, ProjectUser],
    }),
    UserModule,
    ProjectModule,
    ProjectUserModule,
    AuthModule,
  ],
  controllers: [AppController, ProjectController, ProjectUserController, AuthController],
  providers: [AppService],
})
export class AppModule {}
