import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  OneToMany,
  ManyToMany,
} from 'typeorm';
import { ProjectUser } from './project-user.entity';
import { Project } from './projects.entity';

@Entity({
  name: 'users',
})
export class User {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    type: 'varchar',
    length: 255,
    name: 'first_name',
  })
  firstName: string;

  @Column({
    type: 'varchar',
    length: 255,
    name: 'last_name',
  })
  lastName: string;

  @Column({
    type: 'varchar',
    length: 255,
    name: 'user_name',
  })
  userName: string;

  @Column({
    type: 'varchar',
    length: 255,
    name: 'password',
  })
  password: string;

  @Column({
    type: 'tinyint',
    name: 'is_active',
    default: 1,
  })
  isActive: boolean;

  @CreateDateColumn({
    name: 'created_at',
  })
  createdAt: Date;

  @UpdateDateColumn({
    name: 'updated_at',
  })
  updatedAt: Date;

  @OneToMany(() => ProjectUser, (projectUser) => projectUser.userId)
  projectUsers: ProjectUser[];

  @ManyToMany(() => Project, (project) => project.users)
  projects: Project[];
}
