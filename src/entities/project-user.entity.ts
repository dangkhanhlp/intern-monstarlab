import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  ManyToOne,
  JoinColumn,
} from 'typeorm';
import { Project } from './projects.entity';
import { User } from './user.entity';
@Entity({
  name: 'project_users',
})
export class ProjectUser {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    name: 'project_id',
    type: 'integer',
    //unsigned: true,
  })
  @ManyToOne(() => Project, (project) => project.projectUsers)
  @JoinColumn({ name: 'project_id', referencedColumnName: 'id' })
  projectId: number;

  @Column({
    name: 'user_id',
    type: 'integer',
    //unsigned: true,
  })
  @ManyToOne(() => User, (user) => user.projectUsers)
  @JoinColumn({ name: 'user_id', referencedColumnName: 'id' })
  userId: number;

  @CreateDateColumn({
    name: 'created_at',
    nullable: true,
  })
  createdAt: Date;

  @UpdateDateColumn({
    name: 'updated_at',
  })
  updatedAt: Date;

  @DeleteDateColumn({
    name: 'deleted_at',
  })
  deletedAt?: Date;
}
