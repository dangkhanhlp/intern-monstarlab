import { registerDecorator, ValidationOptions } from 'class-validator';

export function IsBiggerThanNow(validationOptions?: ValidationOptions) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      name: 'isBiggerThanNow',
      target: object.constructor,
      propertyName: propertyName,
      options: validationOptions,
      validator: {
        validate(value: any) {
          const startedAt = new Date(value);
          const dateNow = new Date();
          return startedAt > dateNow;
        },
      },
    });
  };
}
