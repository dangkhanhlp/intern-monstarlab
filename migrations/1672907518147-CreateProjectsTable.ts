import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class CreateProjectsTable1672907518147 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.createTable(
      new Table({
        name: 'projects',
        columns: [
          {
            name: 'id',
            type: 'int',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment',
          },
          {
            name: 'name',
            type: 'varchar',
            length: '255',
          },
          {
            name: 'category',
            type: 'varchar',
            default: '"client"',
          },
          {
            name: 'projected_spend',
            type: 'int',
            default: 0,
          },
          {
            name: 'projected_variance',
            type: 'int',
            default: 0,
          },
          {
            name: 'revenue_recognised',
            type: 'int',
            default: 0,
          },
          {
            name: 'project_started_at',
            type: 'datetime',
          },
          {
            name: 'project_ended_at',
            type: 'datetime',
            isNullable: true,
          },
          {
            name: 'created_at',
            type: 'datetime',
            default: 'now()',
          },
          {
            name: 'updated_at',
            type: 'datetime',
            default: 'now()',
          },
          {
            name: 'deleted_at',
            type: 'datetime',
            isNullable: true,
          },
        ],
      }),
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropTable('projects');
  }
}
