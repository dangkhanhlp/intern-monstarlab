import { MigrationInterface, QueryRunner, TableColumn } from 'typeorm';

export class AlterUserTable1677662294100 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.addColumns('users', [
      new TableColumn({
        name: 'user_name',
        type: 'varchar',
        length: '255',
      }),
      new TableColumn({
        name: 'password',
        type: 'varchar',
        length: '255',
      }),
    ]);
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    queryRunner.dropColumn('users', 'user_name');
    queryRunner.dropColumn('users', 'password');
  }
}
